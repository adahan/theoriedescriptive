#### Cours de Théorie Descriptive LMFI 2020

Il manque actuellement le cours du Lundi 17 février. Il faudrait le LaTeXiser, et me l'envoyer par mail, ou bien directement créer un pull request.
De même, n'hésitez pas à corriger les erreurs que vous trouvez.
